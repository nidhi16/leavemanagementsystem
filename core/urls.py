from django.conf.urls import url
from . import views

app_name = 'core'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/team/$', views.add_team_form, name='add_team_form'),
    url(r'^team/detail$', views.team_detail, name='team_detail'),
]
