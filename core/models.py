# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Team(models.Model):
    name = models.CharField(max_length=127, blank=True, null=True)
    description = models.TextField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('team')

    def __unicode__(self):
        return self.name
