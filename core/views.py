# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect


def index(request):
    curr_user = request.user

    if curr_user.is_authenticated():
        return HttpResponseRedirect(reverse('users:dashboard'))
    else:
        return HttpResponseRedirect(reverse('login'))


def add_team_form(request):
    print "In add team form"
    return render(request, 'core/addTeamForm.html')


def team_detail(request):
    pass
