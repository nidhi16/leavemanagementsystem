from __future__ import unicode_literals

from django.shortcuts import render
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse


@login_required
def dashboard(request):
    curr_user = request.user
    if curr_user.is_hr:
        return HttpResponseRedirect(reverse('users:hr_dashboard'))
    elif curr_user.is_manager:
        return HttpResponseRedirect(reverse('users:manager_dashboard'))
    elif curr_user.is_employee:
        return HttpResponseRedirect(reverse('users:employee_dashboard'))
    else:
        return Http404


@login_required
def hr_dashboard(request):
    return render(request, 'users/hr_dashboard.html')


@login_required
def manager_dashboard(request):
    return render(request, 'users/manager_dashboard.html')


@login_required
def employee_dashboard(request):
    return render(request, 'users/employee_dashboard.html')