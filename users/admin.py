# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'type')
    list_filter = ('email', 'type')
    search_fields = ('email', 'first_name', 'last_name')

admin.site.register(User, UserAdmin)
