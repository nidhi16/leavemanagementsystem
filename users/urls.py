from django.conf.urls import url
from . import views

app_name = 'users'

urlpatterns = [
    url(r'^dashboard/', views.dashboard, name='dashboard'),
    url(r'^hr/dashboard/', views.hr_dashboard, name='hr_dashboard'),
    url(r'^manager/dashboard/', views.manager_dashboard, name='manager_dashboard'),
    url(r'^employee/dashboard/', views.employee_dashboard, name='employee_dashboard'),
]
