from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User


# class MyUserCreationForm(UserCreationForm):
#     type = forms.ChoiceField(label='User Type', choices=User.USER_TYPE,
#                                                 widget=forms.Select(attrs={'class': "form-control"}))
#
#     class Meta:
#         model = User
#         fields = ('username', 'email', 'first_name', 'last_name', 'password1', 'password2', 'type')
